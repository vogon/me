# Colin Bayer
Seattle, WA 98115<br/>
contact me at vogon at outlook dot com for additional details 

## Employment Experience 

* **University of Washington**<br/>
  *Research Scientist/Engineer 2, 6/2015-9/2019*<br/>
  Co-lead developer on Mozak (http://mozak.science), which allows laypersons and neuroscientists to collaborate on structural models of neurons in a Web-based computer game.  Worked across the whole stack: a Web client using React and WebGL, a Ruby CMS, and a scientific computing pipeline written in Python and Node.js.  Ensured operations with minimal downtime during the course of deployment, particularly the replacement of several major components.  Observed neuroscientists working with professional tools to inform interaction design; used the results of these observations to design prototype UX which met with overwhelmingly positive feedback.  Adapted research algorithms to production for performance and extensibility.  Worked closely with neuroscientists and players to deliver ongoing new mechanics and functional improvements.
* **University of Washington**<br/>
  *Developer, 12/2013-6/2015*<br/>
  Developed Nanocrafter, a game about synthetic biology, using ActionScript and Flex.  Built new functionality for level editing and peer review of solutions, plus user interface components.  Assisted in maintaining legacy server components while rewriting server code in Ruby for extensibility, security, compatibility with existing client code.  Added new features based on developer and community manager feedback.
* **Microsoft Corporation**<br/>
  *SDE II, 1/2013-9/2013*<br/>
  Built new functionality (search autocomplete and Streetside) for the Windows 8/8.1 Bing Maps client, using C# and XAML.  Developed prototype functionality for a demo to VIPs under time pressure while not slipping production deadlines; worked with management to iterate on requirements multiple times per day based on ongoing feedback.  Integrated complex client code with rapidly-changing Web and local APIs in an environment with extensive dependencies.
* **Microsoft Corporation**<br/>
  *SDE II, 9/2012-1/2013*<br/>
  Built a UI + gameplay prototype for an augmented-reality game using C#, XAML, MonoGame, and internal face-recognition and OCR technology.  Built a proof of concept to demonstrate internal pose-tracking technology.
* **Microsoft Corporation**<br/>
  *SDE/SDE II, 6/2009-9/2012*<br/>
  Built a voice-control application (voice-activated dialing, voice-to-SMS, voice search) for Windows Phone 7, 7.5, and 8 using C++, C#, and an internal UI toolkit.  Designed a system for interoperating securely with voice commands specified and implemented by third parties.  Worked with designers to deliver a modern UX in a resource-constrained environment where prototypes were expensive.  Served as security point-of-contact for a team of ~20 developers.  Wrote and reviewed threat models; assisted other teams with threat modeling.  Collaborated with others in dev and PM to redesign insecure features to reduce security risk while delivering the maximum value to users.
  
## Skills
*Proficient with*: C#, Docker, HTML5/Javascript, Node.js, Python, React/Redux, Ruby, TypeScript, WebGL, speech recognition and synthesis libraries<br/>
*Some experience with*: ActionScript 3, C and C++ on Windows CE/NT and Linux, Java, Kubernetes, SQL, x86 assembly, XNA/MonoGame, bioinformatics libraries and databases